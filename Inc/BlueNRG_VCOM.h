#ifndef __VCOM_H
#define __VCOM_H


#define USARTx                         	USART3													 //USART2
#define USARTx_CLK_ENABLE()            __HAL_RCC_USART3_CLK_ENABLE()     //__USART2_CLK_ENABLE()
#define USARTx_RX_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE
#define USARTx_TX_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE 
#define USARTx_FORCE_RESET()           __USART3_FORCE_RESET()
#define USARTx_RELEASE_RESET()         __USART3_RELEASE_RESET()
/* Definition for USARTx Pins */
#define USARTx_TX_PIN                  GPIO_PIN_10
#define USARTx_TX_GPIO_PORT            GPIOB  
#define USARTx_TX_AF                   GPIO_AF7_USART3;
#define USARTx_RX_PIN                  GPIO_PIN_11
#define USARTx_RX_GPIO_PORT            GPIOB 
#define USARTx_RX_AF                   GPIO_AF7_USART3;
/* Definition for USARTx's NVIC */
#define USARTx_IRQn                    USART3_IRQn
#define USARTx_IRQHandler              USART3_IRQHandler

/* Size of Reception buffer */
#define UARTHEADERSIZE 4
#define RXBUFFERSIZE 255
/**
 * @}
 */
 
/** @addtogroup MAIN_Exported_Macros
 *  @{
 */ 
/* Exported macros -----------------------------------------------------------*/
/* Size of Transmission buffer */
#define TXSTARTMESSAGESIZE               (COUNTOF(aTxStartMessage) - 1)
#define TXENDMESSAGESIZE                 (COUNTOF(aTxEndMessage) - 1)

#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

void MX_BT_COM(void);
void VCOM_main(void);
void VCOM2_main(void);
	
#endif


